<!DOCTYPE HTML>
<html lang="en">

<!-- Mirrored from bootstrap-ecommerce.com/templates/alistyle-html/page-listing-large.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Sep 2020 14:41:47 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="max-age=604800" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Website title - bootstrap html template</title>

<link href="/frontend/images/favicon.ico" rel="shortcut icon" type="image/x-icon">

<!-- jQuery -->
<script src="js/jquery-2.0.0.min.js" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="/frontend/css/bootstrap3661.css?v=2.0" rel="stylesheet" type="text/css"/>
<script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
<!-- Font awesome 5 -->
<link href="/frontend/fonts/fontawesome/css/all.min3661.css?v=2.0" type="text/css" rel="stylesheet">

<!-- custom style -->
<link href="/frontend/css/ui3661.css?v=2.0" rel="stylesheet" type="text/css"/>
<link href="/frontend/css/responsive3661.css?v=2.0" rel="stylesheet" type="text/css" />

<!-- custom javascript -->
<script src="/frontend/js/script3661.js?v=2.0" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="sweetalert2.all.min.js"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src="sweetalert2.min.js"></script>
<link rel="stylesheet" href="sweetalert2.min.css">
</head>

<body>


<header class="section-header">
<section class="header-main border-bottom">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-xl-2 col-lg-3 col-md-12">
				<a href="/" class="brand-wrap">
				<h3 style="color:orange">Elite</h3>
				</a> <!-- brand-wrap.// -->
			</div>
			<div class="col-xl-6 col-lg-5 col-md-6">
				<form action="#" class="search-header">
					<div class="input-group w-100">
						<select class="custom-select border-right"  name="category_name">
								<option value="">All type</option><option value="codex">Special</option>
								<option value="comments">Only best</option>
								<option value="content">Latest</option>
						</select>
					    <input type="text" class="form-control" placeholder="Search">
					    
					    <div class="input-group-append">
					      <button class="btn btn-primary" type="submit">
					        <i class="fa fa-search"></i> Search
					      </button>
					    </div>
				    </div>
				</form> <!-- search-wrap .end// -->
			</div> <!-- col.// -->
			<div class="col-xl-4 col-lg-4 col-md-6">
				<div class="widgets-wrap float-md-right">
					
					<div class="widget-header">
						<a href="/cart" class="widget-view">
							<div class="icon-area">
                                <i class="fa fa-shopping-cart"></i>
                                <span class="notify">@yield('cartcount')</span>
							</div>
							<small class="text"> Cart </small>
						</a>
					</div>
				</div> <!-- widgets-wrap.// -->
			</div> <!-- col.// -->
		</div> <!-- row.// -->
	</div> <!-- container.// -->
</section> <!-- header-main .// -->


</header> <!-- section-header.// -->



<!-- ========================= SECTION CONTENT ========================= -->
@yield('content')

<!-- ========================= SECTION SUBSCRIBE  ========================= -->
<section class="padding-y-lg bg-light border-top">
<div class="container">

<p class="pb-2 text-center">Delivering the latest product trends and industry news straight to your inbox</p>

<div class="row justify-content-md-center">
	<div class="col-lg-4 col-sm-6">
<form class="form-row">
		<div class="col-8">
		<input class="form-control" placeholder="Your Email" type="email">
		</div> <!-- col.// -->
		<div class="col-4">
		<button type="submit" class="btn btn-block btn-warning"> <i class="fa fa-envelope"></i> Subscribe </button>
		</div> <!-- col.// -->
</form>
<small class="form-text">We’ll never share your email address with a third-party. </small>
	</div> <!-- col-md-6.// -->
</div>
	

</div>
</section>
<!-- ========================= SECTION SUBSCRIBE END// ========================= -->


<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-secondary">
	<div class="container">
		<section class="footer-top padding-y-lg text-white">
			<div class="row">
				<aside class="col-md col-6">
					<h6 class="title">Brands</h6>
					<!-- <ul class="list-unstyled">
						<li> <a href="/">Adidas</a></li>
						<li> <a href="/">Puma</a></li>
						<li> <a href="/">Reebok</a></li>
						<li> <a href="/">Nike</a></li>
					</ul> -->
				</aside>
				<aside class="col-md col-6">
					<h6 class="title">Company</h6>
					<ul class="list-unstyled">
						<li> <a href="/">About us</a></li>
						<li> <a href="/">Career</a></li>
						<!-- <li> <a href="/">Find a store</a></li> -->
						<li> <a href="/">Rules and terms</a></li>
						<li> <a href="/">Sitemap</a></li>
					</ul>
				</aside>
				<aside class="col-md col-6">
					<h6 class="title">Help</h6>
					<ul class="list-unstyled">
						<li> <a href="/">Contact us</a></li>
						<li> <a href="/">Money refund</a></li>
						<li> <a href="/">Order status</a></li>
						<li> <a href="/">Shipping info</a></li>
						<li> <a href="/">Open dispute</a></li>
					</ul>
				</aside>
				<aside class="col-md col-6">
					<h6 class="title">Account</h6>
					<ul class="list-unstyled">
						<li> <a href="/"> User Login </a></li>
						<li> <a href="/"> User register </a></li>
						<li> <a href="/"> Account Setting </a></li>
						<li> <a href="/"> My Orders </a></li>
					</ul>
				</aside>
				<aside class="col-md">
					<h6 class="title">Social</h6>
					<ul class="list-unstyled">
						<li><a href="/"> <i class="fab fa-facebook"></i> Facebook </a></li>
						<li><a href="/"> <i class="fab fa-twitter"></i> Twitter </a></li>
						<li><a href="/"> <i class="fab fa-instagram"></i> Instagram </a></li>
						<li><a href="/"> <i class="fab fa-youtube"></i> Youtube </a></li>
					</ul>
				</aside>
			</div> <!-- row.// -->
		</section>	<!-- footer-top.// -->

		<section class="footer-bottom text-center">
		
				<p class="text-white">Privacy Policy - Terms of Use - User Information Legal Enquiry Guide</p>
				<p class="text-muted"> &copy 2020 Company name, All rights reserved </p>
				<br>
		</section>
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->

@yield('script')

</body>

<!-- Mirrored from bootstrap-ecommerce.com/templates/alistyle-html/page-listing-large.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 11 Sep 2020 14:41:47 GMT -->
</html>