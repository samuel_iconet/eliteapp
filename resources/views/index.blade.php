@extends('layouts.frontend')
@section('cartcount')
{{$cartcount}}
@endsection
@section('content')
<section class="section-content padding-y">
<div class="container">


<!-- ============================  FILTER TOP  ================================= -->
<!-- card.// -->
<!-- ============================ FILTER TOP END.// ================================= -->


<div class="row">
	<aside class="col-md-2">

	<article class="filter-group">
		<h6 class="title">
			<a href="/frontend/#" class="dropdown-toggle" data-toggle="collapse" data-target="#collapse_1">	 Product Categories </a>
		</h6>
		<div class="filter-content collapse show" id="collapse_1">
			<div class="inner">
				<ul class="list-menu">
			@foreach($categories as $category)
			<li><a class = "btn btn-warning" href="/{{$category->id}}">{{ $category->name }}</a></li>
				@endforeach
				</ul>
			</div> <!-- inner.// -->
		</div>
	</article> <!-- filter-group  .// -->
<!-- filter-group .// -->
<!-- filter-group .// -->
<!-- filter-group .// -->

	</aside> <!-- col.// -->
	<main class="col-md-10">




	@foreach($products as $product)
	<article class="card card-product-list">
	<div class="row no-gutters">
		<aside class="col-md-3">
			<a href="/frontend/#" class="img-wrap">
				<!-- <span class="badge badge-danger"> NEW </span> -->
				<img src="/storage/product/{{$product->product_image}}">
			</a>
		</aside> <!-- col.// -->
		<div class="col-md-6">
			<div class="info-main">
				<a href="/frontend/#" class="h5 title"> {{$product->name}}</a>
		
                <br><hr><hr>
				<p> {{$product->details}} </p>

			</div> <!-- info-main.// -->
		</div> <!-- col.// -->
		<aside class="col-sm-3">
			<div class="info-aside">
				<div class="price-wrap">
					<span class="h5 price">₦ {{$product->price}}</span> 
					<small class="text-muted">/per item</small>
				</div> <!-- price-wrap.// -->
				<form action="/addtocart" method="post">
				@csrf
				<div class="form-group col-md" >
					<input type="hidden" name="id" value="{{$product->id}}">
					<input type="number" name ="quantity" class="form-control" style= "width:100px" min='1' required>
					<br>
                <button  class="btn  btn-primary" type="submit"> 
                    <i class="fas fa-shopping-cart"></i> <span class="text">Add to cart</span> 
                </button>
               
                 </div> 
			</form>
				

				

			</div> <!-- info-aside.// -->
		</aside> <!-- col.// -->
	</div> <!-- row.// -->
</article> 	
	@endforeach



<nav class="mb-4" aria-label="Page navigation sample">
{!! $products->render() !!}
  <!-- <ul class="pagination">
    <li class="page-item disabled"><a class="page-link" href="/frontend/#">Previous</a></li>
    <li class="page-item active"><a class="page-link" href="/frontend/#">1</a></li>
    <li class="page-item"><a class="page-link" href="/frontend/#">2</a></li>
    <li class="page-item"><a class="page-link" href="/frontend/#">3</a></li>
    <li class="page-item"><a class="page-link" href="/frontend/#">4</a></li>
    <li class="page-item"><a class="page-link" href="/frontend/#">5</a></li>
    <li class="page-item"><a class="page-link" href="/frontend/#">Next</a></li>
  </ul> -->
</nav>
@foreach($data as $d)
<h1>{{$d->name}} {{$d->quantity}}</h1>
@endforeach
	</main> <!-- col.// -->

</div>

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->


@endsection