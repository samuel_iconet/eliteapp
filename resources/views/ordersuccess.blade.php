@extends('layouts.frontend')
@section('cartcount')
{{0}}
@endsection
@section('content')
<div class="row">
<div class="col-4 offset-4">

<div class="card" style="width: 30rem;">
@if($status == "success")
  <img src="/good.png" class="card-img-top" style="height: 350px" alt="...">
  @else
  <img src="/fail.jfif" class="card-img-top" style="height: 350px" alt="...">

  @endif
  <div class="card-body">
   @if($status == "success")
   <h5 class="card-title" style="color:green">Order Successfull</h5>
    <p class="card-text">Your order Have been Successfully placed.</p>
    <h5><span style="color:orange">Order ID</span>   </h5>
    <h6><span class="pull-right" id="name">{{$order_id}}</span> </h6>
    <h5><span style="color:orange">Payment Method </span>   </h5>
    <h6><span class="pull-right" id="name">{{$paymentethod}}</span> </h6>
   @else
   <h5 class="card-title" style="color:red">Order Payment Failed</h5>
    <p class="card-text">Your order Payment Was Not Successfull</p>
    <h5><span style="color:orange">Order ID</span>   </h5>
    <h6><span class="pull-right" id="name">{{$order_id}}</span> </h6>
    <h5><span style="color:orange">Payment Method </span>   </h5>
    <h6><span class="pull-right" id="name">{{$paymentethod}}</span> </h6>
   @endif
  </div>
</div>
</div></div>
@endsection