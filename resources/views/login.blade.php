<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="/login/css/main.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="/login/images/img-01.png" alt="IMG">
				</div>

				<form class="login100-form validate-form">
					<span class="login100-form-title">
						Member Login
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input id="email" class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input id="password" class="input100" type="password" name="pass" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button id="submit" class="login100-form-btn">
							Login
						</button>
					</div>

					

					<div class="text-center p-t-136">
						<a class="txt2" href="/signup">
							Create your Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/login/vendor/bootstrap/js/popper.js"></script>
	<script src="/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/login/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="/login/js/main.js"></script>
    <SCRipt>

$( document ).ready(function() {
    
$( "#submit" ).click(function(e) {
         e.preventDefault();
      $('#loader').show();
      $('#submit').attr('disabled','disabled');
     let email = $('#email').val();
     let password = $('#password').val();
     console.log(email);
     console.log(password);
 $.ajaxSetup({
                 headers: { }
             });
 $.post('/oauth/token',   // url
        {        username: email, 
                 password: password,
                 grant_type: "password",
                 client_id: "2",
                 client_secret: "rm3TPFiLhkyIFYc6EL1j1gxbaJXH6C5JjLqh2pul"
        }, // data to be submit
     // $.post('/api/token',   // url
     //    {        email: email, 
     //             password: password
                
     //    }, // data to be submit
        function(data, status, jqXHR) {// success callback
 
         console.log(status);
                // console.log(data.access_token);
               localStorage.setItem('access_token', data.access_token);
                token2  = data.access_token;
                   console.log(token2);
                  token2 = "Bearer "  + token2;
                   console.log(token2);
                   $.ajax({
                   type: "GET",
                   headers: {'Content-Type': 'application/json', Authorization: token2  },
                   url: "/api/user",
                   success: function(patientDTO) {
                       console.log("SUCCESS: ", patientDTO);
                                   localStorage.setItem('name', patientDTO.username);
                                  localStorage.setItem('email', patientDTO.email);
                                  localStorage.setItem('id', patientDTO.id);
                                  localStorage.setItem('role', patientDTO.role);
                                  localStorage.setItem('status', patientDTO.status);
                                  localStorage.setItem('created_at', patientDTO.created_at);
                                 if(patientDTO.level == 'executive'){
                                    window.location.href = '/executivedashboard';
                                 }else if(patientDTO.level == 'manager'){
                                    window.location.href = '/managerdashboard';
                                 }else if(patientDTO.level == 'admin'){
                                    window.location.href = '/admindashboard';
                                 }else{
                                     console.log("herer")
                                 }
                        },
                   error: function(e) {
                       $('#loader').hide();
                       $('#submit').removeAttr('disabled');
                   // display(e.responseJSON.message);
                   // alert(e);
 
                   }
               });
 
 
 
         }).fail(function(jqxhr, settings, ex) {
            // $('#err').show();
            $('#loader').hide();
            $('#submit').removeAttr('disabled');
            console.log(jqxhr.status);
            if(jqxhr.status == 400){
                // alert("Invalid Email and Password Provided");
                swal({
            title: "Error",
            text: "Invalid Email and Password Provided",
            icon: "error",
            });
            }else{
                // alert("Network Error");
                swal({
            title: "Error",
            text: "Network Error",
            icon: "error",
            });  
            }
          });
 
 
 
 
 });
});
    </SCRipt>
</body>
</html>
