<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login V1</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="/login/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="/login/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="/login/css/main.css">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt>
					<img src="/login/images/img-01.png" alt="IMG">
				</div>

				<form id="target" class="login100-form validate-form" action="/" method="post">
					<span class="login100-form-title">
						Member SignUp
					</span>
                    <div class="wrap-input100 validate-input" data-validate = "Enter your Name">
						<input id="name"  class="input100" type="text" name="name" placeholder="Name" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input id="email" class="input100" type="email" name="email" placeholder="Email" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input  id="password"class="input100" type="password" name="password" placeholder="Password" required>
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" id="submit">
                        <i id="loader" style="display: none" class="fa fa-circle-o-notch fa-spin"></i>SignUp
						</button>
					</div>

					

					<div class="text-center p-t-136">
						<a class="txt2" href="/signin">
							Have an Account
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	

	
<!--===============================================================================================-->	
	<script src="/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/login/vendor/bootstrap/js/popper.js"></script>
	<script src="/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/login/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/login/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="/login/js/main.js"></script>
<script>
 $( document ).ready(function() {

    $( "#target" ).submit(function( event ) {
       
       event.preventDefault();
       $('#loader').show();
       $('#submit').attr('disabled','disabled');
       let email = $('#email').val();
       let username =  $('#name').val() + " " + $('#lname').val();
       let password = $('#password').val();
   
       $.ajaxSetup({
               headers: { }
           });
$.post('/api/register',   // url
      {        username: username, 
               password: password,
               email: email,
               level: 'executive'
               
      }, 
      function(data, status, jqXHR) {// success callback

       console.log(status);
       console.log(data);      
    
       if(data.code == "200"){
           $('#loader').hide();
          $('#submit').removeAttr('disabled');
            swal({
            title: "Registration Successfull ",
            text: "You can Proceed to login",
            icon: "success",
            });
        

       }else{
        swal({
            title: "Error",
            text: data.email[0],
            icon: "error",
            });
          $('#loader').hide();
          $('#submit').removeAttr('disabled');
       }


       }).fail(function(jqxhr, settings, ex) {
        swal({
            title: "Error",
            text: "An Error Occured on the Server",
            icon: "success",
            });
          $('#loader').hide();
          $('#submit').removeAttr('disabled');
         
        });




       });
 });
</script>
</body>
</html>
