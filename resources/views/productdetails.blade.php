@extends('layouts.frontend')
@section('cartcount')
{{$cartcount}}
@endsection
@section('content')
<section class="section-content bg-white padding-y">
    <div class="container">
    
    <!-- ============================ ITEM DETAIL ======================== -->
        <div class="row">
            <aside class="col-md-6">
    <div class="card">
    <article class="gallery-wrap"> 
        <div class="img-big-wrap">
          <div> <a href="#"><img src="/storage/product/{{$product->product_image}}"></a></div>
        </div> <!-- slider-product.// -->
       <!-- slider-nav.// -->
    </article> <!-- gallery-wrap .end// -->
    </div> <!-- card.// -->
            </aside>
            <main class="col-md-6">
    <article class="product-info-aside">
    
    <h2 class="title mt-3">{{$product->name}} </h2>
  <!-- rating-wrap.// -->
    
    <div class="mb-3"> 
        <var class="price h4">₦ {{$product->price}}</var> 
    </div> <!-- price-detail-wrap .// -->
    
    <p>{{$product->details}}</p>
    
    
  
        <div class="form-row  mt-4">
           
        <form action="/addtocart" method="post">
				@csrf
				<div class="form-group col-md" >
					<input type="hidden" name="id" value="{{$product->id}}">
					<input type="number" name ="quantity" class="form-control" style= "width:100px" min='1' required>
					<br>
                <button  class="btn  btn-primary" type="submit"> 
                    <i class="fas fa-shopping-cart"></i> <span class="text">Add to cart</span> 
                </button>
               
                 </div> 
			</form>
        </div> <!-- row.// -->
    
    </article> <!-- product-info-aside .// -->
            </main> <!-- col.// -->
        </div> <!-- row.// -->
    
    <!-- ================ ITEM DETAIL END .// ================= -->
    
    
    </div> <!-- container .//  -->
    </section>

@endsection