@extends('layouts.frontend')
@section('cartcount')
{{$cartcount}}
@endsection
@section('content')
<section class="section-content padding-y">
<div class="container">

<div class="row">
	<main class="col-md-9">
<div class="card">

<table class="table table-borderless table-shopping-cart">
<thead class="text-muted">
<tr class="small text-uppercase">
  <th scope="col">Product</th>
  <th scope="col" width="120">Quantity</th>
  <th scope="col" width="120">Price</th>
  <th scope="col" class="text-right" width="200"> </th>
</tr>
</thead>
<tbody>
@foreach($carts as $cart)
<tr>
	<td>
		<figure class="itemside">
			<div class="aside"><img src="/storage/product/{{$cart->product_image}}" class="img-sm"></div>
			<figcaption class="info">
				<a href="#" class="title text-dark">{{$cart->name}}</a>
			</figcaption>
		</figure>
	</td>
	<td> 
        <h4>{{$cart->quantity}}</h4>
	</td>
	<td> 
		<div class="price-wrap"> 
			<var class="price" style="color:green">₦{{$cart->price * $cart->quantity}}</var> 
			<small class="text-muted"> ₦{{$cart->price}} each </small> 
		</div> <!-- price-wrap .// -->
	</td>
	<td class="text-right"> 
    @if(!$checkout)
    <a href="/removeItem/{{$cart->id}}" class="btn btn-danger" > Remove</a>
    @endif
	</td>
</tr>

@endforeach

</tbody>
</table>

<div class="card-body border-top">
@if(!$checkout)	<a href="#" class="btn btn-primary float-md-right" id="checkout"> Make Purchase <i class="fa fa-chevron-right"></i> </a>@endif
	<a href="/" class="btn btn-light"> <i class="fa fa-chevron-left"></i> Continue shopping </a>
</div>	
</div> <!-- card.// -->

<!-- <div class="alert alert-success mt-3">
	<p class="icontext"><i class="icon text-success fa fa-truck"></i> Free Delivery within 1-2 weeks</p>
</div> -->

	</main> <!-- col.// -->
	<aside class="col-md-3">
	
		<div class="card">
			<div class="card-body">
					<dl class="dlist-align">
					  <dt>Total price:</dt>
					  <dd class="text-right">₦ {{$total}}</dd>
					</dl>
					<dl class="dlist-align">
					  <dt>Shipping:</dt>
					  <dd class="text-right">₦ 0</dd>
					</dl>
					<dl class="dlist-align">
					  <dt>Total:</dt>
					  <dd class="text-right  h5"><strong>₦ {{$total}}</strong></dd>
					</dl>
					<hr>
					<p class="text-center mb-3">
						<img src="/frontend/images/misc/payments.png" height="26">
					</p>
					
			</div> <!-- card-body.// -->
        </div>  <!-- card .// -->
        <div class="card mb-3" id="checkoutwindow">
            
<div class="alert alert-success mt-3">
	<p class="icontext"><i class="icon text-success fa fa-truck"></i>Billing info</p>
</div>
			<div class="card-body">
            @if(count($errors)>0)
			                        <div class="alert alert-danger">
			                          @foreach($errors->all() as $error)
			                          <p>{{$error}}</p>
			                          @endforeach
			                        </div>

			                        @endif
            @if(!$checkout) 
            <form action="/checkout" method="post">
                @csrf
                <div class="form-group">
					<label>Name</label>
					<div class="input-group">
						<input type="text" class="form-control" name="name" placeholder="Name" required>
						
					</div>
                    </div>
                    <div class="form-group">
					<label>Email</label>
					<div class="input-group">
						<input type="email" class="form-control" name="email" placeholder="Email" required>
						
					</div>
                </div>
                
                <div class="form-group">
					<label>Phone Number</label>
					<div class="input-group">
						<input type="number" class="form-control" name="phone_number" placeholder="Phone Number" required>
						
					</div>
                </div>
                <div class="form-group">
					<label>Address</label>
					<div class="input-group">
						<input type="text" class="form-control" name="address" placeholder="Address" required>
						
                    </div>
                    </div>
                    <div class="form-group">
					<label>Location</label>
					<select  id="select" class="custom-select border-right"  name="location" >
                               @foreach($states as $state)
                                <option value="{{$state->name}}">{{$state->name}}</option>
                              @endforeach
                                
						</select>
                    </div>  
                    <div class="form-group">
					<label>Referral Code</label>
					<div class="input-group">
						<input type="text" class="form-control" name="refcode" placeholder="ref code" value="{{$refcode}}" required>
						
                    </div>
                </div>  
				<div class="form-group col-md" >
					<br>
                <button  class="btn  btn-primary" type="submit"> 
                    <i class="fas fa-shopping-cart"></i> <span class="text">CheckOut</span> 
                </button>
               
                 </div> 
			</form>
            <form action="/checkout" method="post" >
            @endif
             @if($checkout)   

       
				<div id= "orderdetails" >
                <h5><span style="color:orange">Name</span>   </h5>
                <h6><span class="pull-right" id="name">{{$order->name}}</span> </h6>
                <h5><span style="color:orange">Email</span>   </h5>
                <h6><span class="pull-right" id="name">{{$order->email}}</span> </h6>
                <h5><span style="color:orange">Phone Number</span>   </h5>
                <h6><span class="pull-right" id="name">{{$order->phone_number}}</span> </h6>
                <h5><span style="color:orange">Address</span>   </h5>
                <h6><span class="pull-right" id="name">{{$order->address}}</span> </h6>
                <h5><span style="color:orange">Location</span>   </h5>
                <h6><span class="pull-right" id="name">{{$order->state}}</span> </h6>
                <h5><span style="color:orange">refcode</span>   </h5>
                <h6><span class="pull-right" id="name">{{$order->refcode}}</span> </h6>
                 </div>
                <div id="cardpayment" style="display: none"> 
                <form method="POST" action="{{ route('pay') }}" accept-charset="UTF-8" class="form-horizontal" role="form">
                <div class="row" style="margin-bottom:40px;">
                    <div class="col-md-8 col-md-offset-2">
                        <p>
                            <div>
                               Order Total
                                ₦ {{$total}}
                            </div>
                        </p>
                        <input type="hidden" name="email" value="{{$order->email}}"> {{-- required --}}
                        <input type="hidden" name="orderID" value="{{$order->id}}">
                        <input type="hidden" name="amount" value="{{$total * 100}}"> {{-- required in kobo --}}
                        <input type="hidden" name="quantity" value="1">
                        <input type="hidden" name="currency" value="NGN">
                        <input type="hidden" name="metadata" value="{{ json_encode($array = ['order_id' => $order->id,]) }}" >
                        <input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}"> {{-- required --}}
                        {{ csrf_field() }} {{-- works only when using laravel 5.1, 5.2 --}}

                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> {{-- employ this in place of csrf_field only in laravel 5.0 --}}

                        <p>
                            <button class="btn btn-success btn-lg btn-block" type="submit" value="Pay Now!">
                                <i class="fa fa-plus-circle fa-lg"></i> Pay Now!
                            </button>
                        </p>
                    </div>
                </div>
            </form>
                </div>

                <div id="payment" >
                   <button class="btn btn-success"  id="paywithcard">Pay with Card</button>
                   @if(isset($pod))
                    @if($pod)
                    <a href="/order/pod/{{$order->id}}" class="btn btn-success" id="pod" >Pay on Delivery</a>
                    @endif
                   @endif
                   </div>
                @endif
                    <hr>
                   
                    
                     <!-- style="display:none" -->
                 
                  
				</div>
			
			</div> <!-- card-body.// -->
		</div>  <!-- card .// -->
	</aside> <!-- col.// -->
</div>

</div> <!-- container .//  -->
</section>
<!-- ========================= SECTION CONTENT END// ========================= -->

<!-- ========================= SECTION  ========================= -->
<section class="section-name border-top padding-y">
<div class="container">
<h6>Payment and refund policy</h6>

</div><!-- container // -->
</section>
@endsection

@section('script')

<script type="text/javascript">

    $(document).ready(function() {
        $('#checkoutbutton').click(function(e){
            e.preventDefault();

        });
        $( "#submit" ).click(function(e) {
         e.preventDefault();
         Swal.fire({
            title: 'Error!',
            text: 'Do you want to continue',
            icon: 'error',
            confirmButtonText: 'Cool'
            })
        });
        $( "#paywithcard" ).click(function(e) {
         e.preventDefault();
         $('#cardpayment').show();   
        });
        $( "#checkout" ).click(function(e) {
         e.preventDefault();
        $('#checkoutwindow').show();
        $('#checkout').hide();
        
        });
        $( "#select" ).change(function() {
            console.log($(this).children("option:selected"). val());

   
 });

    });
   </script>
@endsection
