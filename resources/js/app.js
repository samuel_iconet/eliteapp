
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import Vue from 'vue/dist/vue.common.js'

import VueSweetalert2 from 'vue-sweetalert2';
 
import 'sweetalert2/dist/sweetalert2.min.css';
 
Vue.use(VueSweetalert2);

window.Vue = Vue;
import VueRouter from 'vue-router';
Vue.use(VueRouter);
// import VueRouter from 'vue-router';
// import { Form, HasError, AlertError } from 'vform';
// import VueSweetalert2 from 'vue-sweetalert2';
 
 //import Datetime from 'vue-datetime';
//import 'vue-datetime/dist/vue-datetime.css';
//  import datePicker from 'vue-bootstrap-datetimepicker';
//   import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
//   Vue.use(datePicker);
// window.datePicker = datePicker;



// Vue.use(Datetime);
// window.Datetime = Datetime;
const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674'
}
 
// Vue.use(VueSweetalert2, options);

// window.axios = require('axios');
// window.swal = VueSweetalert2;
// window.Form = Form;
// window.email = "";
// window.username = "";

// window.Fire = Fire;
// Vue.use(VueRouter)
// Vue.config.devtools = false;

let Fire = new Vue();
window.Fire = Fire;



let routes = [
 { path: '/dashboard', component: require('./components/dashboard.vue').default },
 { path: '/admindashboard', component: require('./components/admindashboard.vue').default },
 { path: '/artisandashboard', component: require('./components/artisandashboard.vue').default },
 { path: '/supervisordashboard', component: require('./components/supervisordashboard.vue').default },
 { path: '/userdashboard', component: require('./components/userdashboard.vue').default },
 { path: '/getuserrequests', component: require('./components/userrequestdata.vue').default },
 { path: '/getmergerequest', component: require('./components/artisanmerge.vue').default },
 { path: '/proofs', component: require('./components/proofs.vue').default },
 { path: '/artisandata', component: require('./components/artisandata.vue').default },
 { path: '/usermanagement', component: require('./components/usermanagement.vue').default },
 { path: '/requestmanagent', component: require('./components/artisanrequest.vue').default },
 { path: '/mergedrequest', component: require('./components/adminmergedrequest.vue').default },
 { path: '/completedrequest', component: require('./components/admincompletedrequest.vue').default },
 { path: '/productmanagement', component: require('./components/productmanagement.vue').default },
 { path: '/storerequest', component: require('./components/storerequest.vue').default },
 { path: '/artisanskills', component: require('./components/artisanskills.vue').default },
 { path: '/logout', component: require('./components/logout.vue').default },
 { path: '/pass', component: require('./components/newcomp.vue').default },
 { path: '/test', component: require('./components/testcomp.vue').default },
 { path: '/newpassword', component: require('./components/managepassword.vue').default },
 { path: '/tips', component: require('./components/tips.vue').default },
 { path: '/supervisordata', component: require('./components/supervisordata.vue').default },
 { path: '/supervisormerge', component: require('./components/supervisormerge.vue').default },
 { path: '*', component: require('./components/404.vue').default  }
 
]

const router = new VueRouter({
	mode: 'history',
  routes // short for `routes: routes`
})

const app = new Vue({

    el: '#app',
    router
});
