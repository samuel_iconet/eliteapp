<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// Authentcation routeattachTomanager
Route::post('/register', 'apiAuthController@register');
Route::get('/verifystatepod/{state}', 'apiAuthController@verifystatepod');
Route::get('/adminauth/error','apiAuthController@adminAuthError' );

// user management
Route::post('/attachTomanager', 'adminUsercontroller@attachTomanager');
Route::post('/assignGroup', 'adminUsercontroller@assignGroup');
Route::post('/asynlevel', 'adminUsercontroller@asynlevel');
Route::post('/downgradeManager', 'adminUsercontroller@downgradeManager');
Route::post('/upgradeUser', 'adminUsercontroller@upgradeUser');
Route::get('/getManagersbygroup/{group_id}', 'adminUsercontroller@getManagersbygroup');
Route::get('/getDownlines/{manager_id}', 'adminUsercontroller@getDownlines');
Route::get('/getExecutives', 'adminUsercontroller@getExecutives');
Route::get('/getManagers', 'adminUsercontroller@getManagers');
Route::get('/getadmins', 'adminUsercontroller@getadmins');


//category management

Route::post('/addCategory', 'categoryController@addCategory');
Route::post('/editCatergory', 'categoryController@editCatergory');
Route::get('/category/flipStatus/{id}', 'categoryController@flipStatus');

//product management createOrder

Route::get('/getProducts', 'productController@getProducts');
Route::get('/product/flipstatus/{id}', 'productController@flipStatus');
Route::post('/createProduct', 'productController@createProduct');
Route::post('/editproduct', 'productController@editProduct');
Route::post('/product/changeimage', 'productController@changeImage');

// order placement 
Route::post('/createorder', 'orderController@createOrder');


/// admin order controller

Route::get('/getorders', 'adminOrderController@getOrders');
Route::get('/getorderbyexecutive/{id}', 'adminOrderController@getOrderbyExecutive');
Route::get('/getorderbymanager/{id}', 'adminOrderController@getOrderbyManager');
Route::get('/getorderbygroup/{id}', 'adminOrderController@getOrderbygroup');
Route::get('/getorderbytype/{type}', 'adminOrderController@getOrderbyType');
Route::get('/confirmdelivery/{id}', 'adminOrderController@confirmdelivery');
Route::get('/cancelorder/{id}', 'adminOrderController@cancelOrder');



