<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/ordersuccess', function () {
    return view('ordersuccess' , ['order_id' => 2 , 'paymentethod' => 'card']);
});
Route::get('/signin', function () {
    return view('login');
});
Route::get('/signup', function () {
    return view('register');
});
Route::get('/', 'frontendcontroller@home');
Route::get('/addtocart/{id}/{quantity}', 'frontendcontroller@getaddToCart');
Route::get('/productdetail/{id}', 'frontendcontroller@productdetails');
Route::post('/addtocart', 'frontendcontroller@addToCart');
Route::post('/checkout', 'frontendcontroller@checkout');
Route::get('/removeItem/{id}', 'frontendcontroller@removeItem');
Route::get('/cart', 'frontendcontroller@getCart');
Route::get('/ref/{code}', 'frontendcontroller@addreflink');
Route::get('/order/pod/{id}', 'orderController@pod');

Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');

Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');
Route::get('/notfound', function () {
    return view('notfound');
});
Route::get('{path}',"HomeController@index")->where( 'path', '([A-z\d\-\/_.]+)?' );