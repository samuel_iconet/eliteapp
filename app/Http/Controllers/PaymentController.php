<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paystack;
use Redirect;
use App\checkorder;
use App\User;
use App\group;
use App\credithistory;
use App\order;
use App\account;
class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */
    public function redirectToGateway()
    {
        try{
            return Paystack::getAuthorizationUrl()->redirectNow();
        }catch(\Exception $e) {
            return redirect('/');
        }        
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        $paymentDetails = Paystack::getPaymentData();
         $checkout_id = $paymentDetails['data']['metadata']['order_id'];
       if($paymentDetails['status'] == 'success'){
        if(isset($checkout_id)){
            $checkout = checkorder::findOrFail($checkout_id);
            $balance = 0;
            $referrer = user::where(['refcode' => $checkout->refcode , 'level' => 'executive' ])->first();
            if(isset($referrer)){
                $groupshare = ((1/100)*$checkout->amount);
                $managershare = ((4/100)*$checkout->amount);
                $executiveshare = ((25/100)*$checkout->amount);
      
                //order
                $order = new order;
                $order->name = $checkout->name;
                $order->type = "card";
             
                $order->payment_status = 'completed';
                $order->status = 'pending delivery';
                $order->cart = $checkout->cart;
                $order->address = $checkout->address;
                $order->email = $checkout->email;
                $order->phone_number = $checkout->phone_number;
                $order->state = $checkout->state;
                $order->amount = $checkout->amount;
                $order->balance = ((70/100) * $checkout->amount);
                $order->refcode = $checkout->refcode;
                $order->executive = $referrer->id;
                $order->manager = $referrer->up_line;
                $order->group_id = $referrer->group_id;
                $order->save();
              
            $group_account = group::find($referrer->group_id);
            $group_account->balance = $group_account->balance + $groupshare;
            $group_account->save();
            $credithistory = new credithistory;
            $credithistory->type = 'group';
            $credithistory->user_id = $referrer->group_id;
            $credithistory->amount = $groupshare;
            $credithistory->order_id = $order->id; //pending
            $credithistory->save();
    
            $executive_account = account::where('user' , $referrer->id)->first();
            $executive_account->balance = $executive_account->balance + $executiveshare;
            $executive_account->save();
            $credithistory->type = 'user';
            $credithistory->amount = $executiveshare;
            $credithistory->user_id = $referrer->id;
            $credithistory->order_id = $order->id; //pending
            $credithistory->save();
    
            $manager_account = account::where('user' , $referrer->up_line)->first();
            $manager_account->balance = $manager_account->balance + $managershare;
            $manager_account->save();
            $credithistory->type = 'user';
            $credithistory->amount = $managershare;
            $credithistory->user_id = $referrer->up_line;
            $credithistory->order_id = $order->id; //pending
            $credithistory->save();
               
                
             
            }  else{
               $order = new order;
               $order->name = $checkout->name;
               $order->type = "CARD";
            
               $order->payment_status = 'completed';
               $order->status = 'pending delivery';
               $order->cart = $checkout->cart;
               $order->address = $checkout->address;
               $order->state = $checkout->state;
               $order->amount = $checkout->amount;
               $order->balance = ( $checkout->amount);
               $order->refcode = $checkout->refcode;
               $order->executive = "0";
               $order->manager = "0";
               $order->group_id = "0";
               $order->save();
            }
            return view('ordersuccess' , ['status' => 'success' ,'order_id' => $order->id , 'paymentethod' => 'CARD']);
        } return view('ordersuccess' , ['status' => 'success' ,'order_id' => 'null' , 'paymentethod' => 'CARD']);
       }
       return view('ordersuccess' , ['status' => 'failed' ,'order_id' => $checkout_id , 'paymentethod' => 'CARD']); 
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
    }
}