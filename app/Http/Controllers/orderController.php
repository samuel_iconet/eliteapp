<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\group;
use App\credithistory;
use App\order;
use App\checkorder;
use App\account;
use Validator;
class orderController extends Controller
{
    //
    public function createOrder(request $request){
        $validator = Validator::make($request->all(), [
            "type" =>  "required",
            "name" =>  "required",
            "chart" => "required",
            "address" => "required",
            "state" => "required",
            "amount" => "required",
            "refcode" => "required"
            
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      $balance = 0;
      $referrer = user::where(['refcode' => $request->refcode , 'level' => 'executive' ])->first();
      if(isset($referrer)){
          $groupshare = ((1/100)*$request->amount);
          $managershare = ((4/100)*$request->amount);
          $executiveshare = ((25/100)*$request->amount);

          //order
          $order = new order;
          $order->name = $request->name;
          $order->type = $request->type;
          if($request->type == 'POD'){
            $order->payment_status = 'pending';
            $order->status = 'pending delivery';
            $order->chart = $request->chart;
            $order->address = $request->address;
            $order->state = $request->state;
            $order->amount = $request->amount;
            $order->balance =  $request->amount;
            $order->refcode = $request->refcode;
            $order->refcode = $request->refcode;
            $order->executive = $referrer->id;
            $order->manager = $referrer->up_line;
            $order->group_id = $referrer->group_id;
            $order->save();
          }
          else{

            $order->payment_status = 'completed';
            $order->status = 'pending delivery';
            $order->chart = $request->chart;
            $order->address = $request->address;
            $order->state = $request->state;
            $order->amount = $request->amount;
            $order->balance = ((70/100) * $request->amount);
            $order->refcode = $request->refcode;
            $order->refcode = $request->refcode;
            $order->executive = $referrer->id;
            $order->manager = $referrer->up_line;
            $order->group_id = $referrer->group_id;
            $order->save();
            $order->status = 'pending delivery';
          $order->chart = $request->chart;
          $order->address = $request->address;
          $order->state = $request->state;
          $order->amount = $request->amount;
          $order->balance = ((70/100) * $request->amount);
          $order->refcode = $request->refcode;
          $order->refcode = $request->refcode;
          $order->executive = $referrer->id;
          $order->manager = $referrer->up_line;
          $order->group_id = $referrer->group_id;
          $order->save();
        $group_account = group::find($referrer->group_id);
        $group_account->balance = $group_account->balance + $groupshare;
        $group_account->save();
        $credithistory = new credithistory;
        $credithistory->type = 'group';
        $credithistory->user_id = $referrer->group_id;
        $credithistory->amount = $groupshare;
        $credithistory->order_id = $order->id; //pending
        $credithistory->save();

        $executive_account = account::where('user' , $referrer->id)->first();
        $executive_account->balance = $executive_account->balance + $executiveshare;
        $executive_account->save();
        $credithistory->type = 'user';
        $credithistory->amount = $executiveshare;
        $credithistory->user_id = $referrer->id;
        $credithistory->order_id = $order->id; //pending
        $credithistory->save();

        $manager_account = account::where('user' , $referrer->up_line)->first();
        $manager_account->balance = $manager_account->balance + $managershare;
        $manager_account->save();
        $credithistory->type = 'user';
        $credithistory->amount = $managershare;
        $credithistory->user_id = $referrer->up_line;
        $credithistory->order_id = $order->id; //pending
        $credithistory->save();
          }
          
        $response['code'] = 200;
        return response()->json($response ,200);  
      } 
    }
    public function pod($id){
      $checkout = checkorder::findOrFail($id);
      $order = new order;
      $order->payment_status = 'pending';
      $order->status = 'pending delivery';
      $order->cart = $checkout->cart;
      $order->type = 'POD';
      $order->address = $checkout->address;
      $order->state = $checkout->state;
      $order->amount = $checkout->amount;
      $order->email = $checkout->email;
      $order->name = $checkout->name;
      $order->phone_number = $checkout->phone_number;
      $order->balance =  $checkout->amount;
      $order->refcode = $checkout->refcode;
      $referrer = user::where(['refcode' => $checkout->refcode , 'level' => 'executive' ])->first();
      if(isset($referrer)){
        $order->executive = $referrer->id;
      $order->manager = $referrer->up_line;
      $order->group_id = $referrer->group_id;
      }else{
        $order->executive ='0';
      $order->manager = '0';
      $order->group_id = '0';
      }
      
      $order->save();
      return view('ordersuccess' , ['status' => 'success' ,'order_id' => $order->id , 'paymentethod' => 'Pay on Delivery']);

    }
}
