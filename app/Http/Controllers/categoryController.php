<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use Validator;
class categoryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        // $this->middleware('admin');

    }
    public function addCategory(request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required|string|max:255|unique:categories",
           
      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $category = new category;
      $category->name = $request->name;  
      $category->status = '1';  
      $category->save();
      $response['code'] = 200;
      return response()->json($response ,200);   
    }
    public function flipStatus($id){
        $category = category::findOrFail($id);
        if($category->status == '1'){
            $category->status = '0';
        }else{
            $category->status = '1';   
        }
        $category->save();
        $response['code'] = 200;
        return response()->json($response ,200);  
    }
    public function editCatergory(request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required",
            "id" =>  "required",
           
      ]);
      if ($validator->fails()) {

        return $validator->messages();
         }

        $category = category::findOrFail($request->id);
        $category->name = $request->name;
        $category->save();
        $response['code'] = 200;
        return response()->json($response ,200); 
    }
}
