<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\category;
use App\product;
use App\User;
use App\checkorder;
use App\state;
use Validator;
class frontendcontroller extends Controller
{
    //
    public function home(){
        $categories = category::where('status' , '1')->get();
        $products = product::where('status' , '1')->paginate(10);
        $value = session('cart', '[]');
        $refcode = session('refcode', '');
        if($value == '[]'){
            $data = array();

        }else{
            $data = unserialize($value);
        }
        $cartcount = count($data);
        return view('index' ,['categories' => $categories , 'products' => $products ,'data' => $data , 'cartcount' => $cartcount]);
    }
    public function getCart(){
        $states = state::all();
        $value = session('cart', '[]');
        $refcode = session('refcode', '');
        if($value == '[]'){
            $data = array();
            return redirect()->back();
        }else{
            $data = unserialize($value);
            $cartcount = count($data);
            $total = 0;
            foreach($data as $cart){
                $total += $cart->price * $cart->quantity;
            }
            $checkout = false;
            return view('cart' ,['states' => $states , 'carts' => $data , 'cartcount' => $cartcount , 'refcode' => $refcode ,'total' => $total , 'checkout' =>  $checkout]);
        
        }
    }
    public function productdetails($id){
        $product = product::find($id);
        if(isset($product)){
            $value = session('cart', '[]');
            $refcode = session('refcode', '');
        if($value == '[]'){
            $data = array();
            
        }else{
            $data = unserialize($value);
        }
        $cartcount = count($data); 
        return view('productdetails' ,['product' => $product , 'cartcount' => $cartcount]);
        }else{
            return redirect()->back();
        }
    }
    public function addToCart(request $request){
        $id= $request->id;
        $quantity= $request->quantity;
        $item  = product::findorFail($id);
        $value = session('cart', '[]');
        if($value == '[]'){
            $cart =array();
            $item['quantity'] = $quantity;
            $cart[0] = $item;
          $data = serialize($cart);
            
          session(['cart' => $data]);
        return redirect()->back();
        }else{
            $value = session('cart', '[]');
           $data = unserialize($value);
           $pos = count($data);
           $exist = false;
           $keypos =0;
           foreach($data as $key=>$value){
            if($value->id == $item->id){
                $exist= true;
                $keypos = $key;
            }   
           }
           if($exist){
               $data[$keypos]['quantity'] =  $quantity;
           }else{
            $item['quantity'] =$quantity;
            $data[$pos] =  $item; 
           }
         $output =  serialize($data);
         session(['cart' => $output]);
         return redirect()->back();

        }
      
       
    }
    public function getaddToCart($id , $quantity){
        $item  = product::findorFail($id);
        $value = session('cart', '[]');
        if($value == '[]'){
            $cart =array();
            $item['quantity'] = $quantity;
            $cart[0] = $item;
          $data = serialize($cart);
            
          session(['cart' => $data]);
        return 'done new';
        }else{
            $value = session('cart', '[]');
           $data = unserialize($value);
           $pos = count($data);
           $exist = false;
           $keypos =0;
           foreach($data as $key=>$value){
            if($value->id == $item->id){
                $exist= true;
                $keypos = $key;
            }   
           }
           if($exist){
               $data[$keypos]['quantity'] =  $quantity;
           }else{
            $item['quantity'] =$quantity;
            $data[$pos] =  $item; 
           }
         $output =  serialize($data);
         session(['cart' => $output]);
         return 'down inc';
        }
      
       
    }
    public function removeItem($id){
        $item  = product::findorFail($id);
        $value = session('cart', '[]');
        if($value == '[]'){
         
            return redirect()->back();

        }else{
            $value = session('cart', '[]');
           $data = unserialize($value);
           $pos = count($data);
           $exist = false;
           $keypos =0;
           foreach($data as $key=>$value){
            if($value->id == $item->id){
                $exist= true;
                $keypos = $key;
            }   
           }
           if($exist){
           unset($data[$keypos]);
           $output =  serialize($data);
           session(['cart' => $output]);
           return redirect()->back();


           }else{
            return redirect()->back();

           
           }
       
        }   
    }
    public function addreflink($code){
        $user = user::where(['level' => 'executive' , 'refcode' => $code])->first();
        if(isset($user)){
            session(['refcode' => $code])  ;
            return redirect('/');  
        }

    }
    
    Public function checkout(request $request){
     
      $this->validate($request,[
        "name" =>  "required",
        "address" => "required",
        "email" => "required",
        "phone_number" => "required",
        "location" => "required",
        "refcode" => "required"
     
        ]);
      $value = session('cart', '[]');
        if($value == '[]'){
            return redirect('/');
        }else{
        $data = unserialize($value); 
            $total = 0;
        foreach($data as $cart){
            $total += $cart->price * $cart->quantity;
        }
        $order = new checkorder;
         $order->name = $request->name;   
         $order->address = $request->address;   
         $order->refcode = $request->refcode;   
         $order->email = $request->email;   
         $order->phone_number = $request->phone_number;   
         $order->state = $request->location;   
         $order->amount = $total;   
         $order->status = '1';   
         $order->payment_status = '0';   
         $order->cart = $value; 
         $order->save(); 
         session(['cart' => '[]']); 
         $checkout = true;
         $cartcount = 0;
         $pod = false;
         $state = state::where('name' ,  $request->location)->first();
         if(isset($state) && $state->allow_pod == '1'){
         $pod = true;
         }else{
            $pod = false;
         }
         return view( 'cart' ,['pod' => $pod ,'order'=> $order ,'carts' => $data , 'cartcount' => $cartcount , 'refcode' => $order->refcode ,'total' => $total , 'checkout' =>  $checkout]);
     
        }
    }
}
