<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use App\group;
use Validator;

class adminUsercontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        // $this->middleware('admin');

    }
    public function getManagersbygroup($group_id){
        $managers = user::where(['level' => 'manager' , 'group_id' => $group_id])->get();
        $response['code'] = 200;
        $response['users'] = $managers;
    return response()->json($response ,200); 
    }
    public function getDownlines($manager_id){
        $users = user::where('up_line' , $manager_id)->get();
        $response['code'] = 200;
        $response['users'] = $users;
    return response()->json($response ,200);
    }
    public function getExecutives(){
        $executives = user::where('level' , 'executive')->get();
        $response['code'] = 200;
        $response['users'] = $executives;
    return response()->json($response ,200);
    }
    public function getManagers(){
        $managers = user::where('level' , 'manager')->get();
        $response['code'] = 200;
        $response['users'] = $managers;
    return response()->json($response ,200);
    }
    public function getadmins(){
        $admins = user::where('level' , 'admin')->get();
        $response['code'] = 200;
        $response['users'] = $admins;
    return response()->json($response ,200);
    }
    public function asynlevel(request $request){
        $validator = Validator::make($request->all(), [
            
            "level" =>  "required",
            "user_id" =>  "required",

      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
    $user = User::findOrFail($request->user_id);
    $user->level = $request->level;
    $user->save();
    $response['code'] = 200;
    return response()->json($response ,200);
    }
    public function attachTomanager(request $request){
        $validator = Validator::make($request->all(), [
            
            "manager_id" =>  "required",
            "user_id" =>  "required",

      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $manager = user::findOrFail($request->manager_id);
      $user = user::findOrFail($request->user_id);
      if($manager->group_id != null){
        if($user->level != 'manager' || $user->level != 'admin' ){
           $user->level = 'executive';
           $user->up_line = $manager->id;
           $user->group_id = $manager->group_id;
           $user->save();  
           $response['code'] = 200;
           return response()->json($response ,200);   
        }else{
        $response['code'] = 404;
        $response['error'] = "A manager  cant be assigned to a manager, please do a downgrade.";
        return response()->json($response ,200);   
        }
      }else{
        $response['code'] = 404;
        $response['error'] = "Manager must be assigned to a group";
        return response()->json($response ,200);  
      }
    }
    public function assignGroup(request $request){
        $validator = Validator::make($request->all(), [
            
            "manager_id" =>  "required",
            "group_id" =>  "required",

      ]);

      if ($validator->fails()) {

           return $validator->messages();
      }
      $manager  =  user::findOrFail($request->manager_id);   
      $group  =  group::findOrFail($request->group_id); 
      if($manager->level == 'manager'){
        $manager->group_id = $group->id;
        $manager->save();
        $users = user::where('up_line' , $manager->id) ->get();
        foreach($users as $user){
            $user->group_id = $group->id;
            $user->save();
        }
        $response['code'] = 200;
        return response()->json($response ,200);   
      }else{
        $response['code'] = 404;
        $response['error'] = "can only Assign group to a manager";
        return response()->json($response ,200);  
      } 
    
    }
    public function downgradeManager(request $request){
        $validator = Validator::make($request->all(), [
            
            "manager_id" =>  "required",
            "new_manager_id" =>  "required",
            

      ]);

      if ($validator->fails()) {

           return $validator->messages();
      } 
      $manager  =  user::findOrFail($request->manager_id);  
      $newmanager  =  user::findOrFail($request->new_manager_id);  
      if($manager->level == 'manager' && $newmanager->level == 'manager'){
        if($newmanager->group_id !=null){
          $manager->level = 'executive';
          $users = user::where('up_line' , $manager->id)->get();
          foreach($users as $user){
            $user->group_id = $newmanager->group_id;
            $user->up_line = $newmanager->id;
            $user->save();
        }
        $manager->save();
        $response['code'] = 200;
        return response()->json($response ,200);  
        }
        else{
          $response['code'] = 404;
          $response['error'] = "New Manager Must Have a Group";
          return response()->json($response ,200);  
        } 
          
      }
      else{
        $response['code'] = 404;
        $response['error'] = "User not a manager";
        return response()->json($response ,200);  
      } 
    }
    public function upgradeUser(request $request){
        $validator = Validator::make($request->all(), [
            "group_id"=> 'required',
            "user_id" =>  "required",
            

      ]);
      $user  =  user::findOrFail($request->user_id);  
      $group  =  group::findOrFail($request->group_id);  
      if($user->level == 'executive'){
        $user->level = 'manager';
        $user->group_id = $group->id;
        $user->save();
        $response['code'] = 200;
        return response()->json($response ,200);   

      }else{
        $response['code'] = 404;
        $response['error'] = "User not an Executive";
        return response()->json($response ,200);   
      }
    }
}
