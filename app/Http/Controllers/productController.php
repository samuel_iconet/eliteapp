<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\product;
use Validator;
class productController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api');
        // $this->middleware('admin');

    }
    public function getProducts(){
        $products = product::all();
        $response['code'] = 200;
        $response['products'] = $products;
        return response()->json($response ,200); 
    }
    public function getProductsByCategory($id){
        $products = product::where('category_id' ,  $id)->get();
        $response['code'] = 200;
        $response['products'] = $products;
        return response()->json($response ,200); 
    }

    public function createProduct(request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required|string|max:255|unique:products",
            'category_id' => 'required',
            'price' => 'required',
            'details' =>  'required',
            'file' => 'required',
      ]);

      if ($validator->fails()) {
           return $validator->messages();
      }
      if ($request->has('file')) {
        $image = $request->file('file');
        $name = 'profile'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file->storeAs('product', $filePath, 'public');
        $image_name = $filePath;

        $product = new product;
        $product->name = $request->name;
        $product->category_id = $request->category_id;
        $product->product_image = $image_name;
        $product->price = $request->price;
        $product->details = $request->details;
        $product->status = '1';
        $product->save();
        $response['code'] = 200;
        return response()->json($response ,200); 
    }
    else{
        $response['code'] = 404;
        $response['error'] = "Please Provide a product Image";
        return response()->json($response ,200);  
    }
      
    }
    public function flipStatus($id){
        $product = product::findOrFail($id);
        if($product->status == '1'){
            $product->status = '0';
        }else{
            $product->status = '1';

        }
        $product->save();
        $response['code'] = 200;
        return response()->json($response ,200);  
    }
    public function editProduct(request $request){
        $validator = Validator::make($request->all(), [
            "name" =>  "required|string|max:255|unique:products",
            'category_id' => 'required',
            'price' => 'required',
            'details' =>  'required',
            'product_id'=> 'required'
      ]);

      if ($validator->fails()) {
           return $validator->messages();
      }
      $product = product::findOrFail($request->product_id);
      $product->name = $request->name;
      $product->category_id = $request->category_id;
      $product->price = $request->price;
      $product->details = $request->details;
      $product->save();
      $response['code'] = 200;
      return response()->json($response ,200);   
    }
    public function changeImage(request $request){
        $validator = Validator::make($request->all(), [
          
            'product_id' =>  'required',
            'file' => 'required',
      ]);

      if ($validator->fails()) {
           return $validator->messages();
      }
      if ($request->has('file')) {
        $image = $request->file('file');
        $name = 'profile'.'_'.time();
        $filePath =  $name. '.' . $image->getClientOriginalExtension();
        $request->file->storeAs('product', $filePath, 'public');
        $image_name = $filePath;
        $product = product::findOrFail($request->product_id);
        $product->product_image = $image_name;
        $product->save();
        $response['code'] = 200;
        return response()->json($response ,200); 
      }
      else{
        $response['code'] = 404;
        $response['error'] = "Please Provide a product Image";
        return response()->json($response ,200);  
    }
    }
}
