<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Str;
use App\User;
use App\state;

// use App\Mail\WelcomeMail;
// use Illuminate\Support\Facades\Mail;

class apiAuthController extends Controller
{
  
   
    public function register(request $request){
        $validator = Validator::make($request->all(), [
                    "username" =>  "required",
                    'email' => 'required|string|email|max:255|unique:users',
                    'password' => 'required|string|min:6|',
                    'level' =>  'required'
              ]);
       
              if ($validator->fails()) {
       
                   return $validator->messages();
              }
              else {
               $user = new User;
        
           $user->status = 0;
           $user->name = $request->username;
           $user->level = $request->level;
           $user->email = $request->email;
           $user->refcode = Str::random(8);
           $user->password = bcrypt($request->password);
           $user->save();
       
       
           $response['code'] = 200;
           $response['user'] = $user;
        //  Mail::to($user)->send(new WelcomeMail($user));
           return response()->json($response ,200);
              }
       
         }
         public function adminAuthError(){
            $response['error'] = "User Not Permitted";
            $response['code']  = "401";
            return response()->json($response ,200);
         }
  
     public function verifystatepod($state){
        $state = state::where('name' , $state)->first();
        if(isset($state) && $state->allowpod == '1'){
         $response['code'] = 200;
         return response()->json($response ,200); 
        }else{
         $response['code'] = 301;
         return response()->json($response ,404); 
        }
     }
}
