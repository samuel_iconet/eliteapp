<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\order;

use App\User;
use App\group;
use App\credithistory;
use App\account;
use Validator;
class adminOrderController extends Controller
{
    //
    public function getOrders(){
        $orders = order::all();
        $response['code'] = 200;
        $response['orders'] = $orders;
        return response()->json($response ,200);  
    }
    public function getOrderbyExecutive($id){
        $orders = order::where('executive' , $id)->get();
        $response['code'] = 200;
        $response['orders'] = $orders;
        return response()->json($response ,200);  
    }
    public function getOrderbyManager($id){
        $orders = order::where('manager' , $id)->get();
        $response['code'] = 200;
        $response['orders'] = $orders;
        return response()->json($response ,200);  
    }
    public function getOrderbygroup($id){
        $orders = order::where('group_id' , $id)->get();
        $response['code'] = 200;
        $response['orders'] = $orders;
        return response()->json($response ,200);  
    }
    public function getOrderbyType($type){
        $orders = order::where('type' , $type)->get();
        $response['code'] = 200;
        $response['orders'] = $orders;
        return response()->json($response ,200);  
    }
    public function confirmdelivery($id){
        $order = order::findOrFail($id);
       if($order->status != 'delivered'){
        if($order->type == 'POD'){
            $groupshare = ((1/100)*$order->amount);
            $managershare = ((4/100)*$order->amount);
            $executiveshare = ((25/100)*$order->amount);
            $order->payment_status = 'completed';
            $order->status = 'delivered';
            $order->balance = ((70/100) * $order->amount);
            $order->save();
        $group_account = group::find($order->group_id);
        $group_account->balance = $group_account->balance + $groupshare;
        $group_account->save();
        $credithistory = new credithistory;
        $credithistory->type = 'group';
        $credithistory->user_id = $order->group_id;
        $credithistory->amount = $groupshare;
        $credithistory->order_id = $order->id; //pending
        $credithistory->save();

        $executive_account = account::where('user' , $order->executive)->first();
        $executive_account->balance = $executive_account->balance + $executiveshare;
        $executive_account->save();
        $credithistory->type = 'user';
        $credithistory->amount = $executiveshare;
        $credithistory->user_id = $order->executive;
        $credithistory->order_id = $order->id; //pending
        $credithistory->save();

        $manager_account = account::where('user' ,$order->manager)->first();
        $manager_account->balance = $manager_account->balance + $managershare;
        $manager_account->save();
        $credithistory->type = 'user';
        $credithistory->amount = $managershare;
        $credithistory->user_id = $order->manager;
        $credithistory->order_id = $order->id; //pending
        $credithistory->save();
        }else{
            $order->status = 'delivered';
            $order->save(); 
        }
        $response['code'] = 200;
        return response()->json($response ,200);  
       }else{
        $response['code'] = 404;
        $response['error'] = "Order already confirmed";
        return response()->json($response ,200);  
       }

    }
    public function cancelOrder($id){
        $order = order::findOrFail($id);
        if($order->status != 'delivered'){
         if($order->type == 'POD'){
            $order->payment_status = 'failed';
            $order->status = 'cancelled'; 
            $order->save();
            $response['code'] = 200;
        return response()->json($response ,200);  
         }
         else{
            $response['code'] = 404;
            $response['error'] = "Cant Cancel an Order Paid With Card.";
            return response()->json($response ,200);  
           }
    }
    else{
        $response['code'] = 404;
        $response['error'] = "Order already confirmed";
        return response()->json($response ,200);  
       }

}
}