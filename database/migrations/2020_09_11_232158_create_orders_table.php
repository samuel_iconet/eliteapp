<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // $order->status = 'pending delivery';
    // $order->chart = $request->chart;
    // $order->address = $request->address;
    // $order->state = $request->state;
    // $order->amount = $request->amount;
    // $order->balance = ((70/100) * $request->amount);
    // $order->refcode = $request->refcode;
    // $order->refcode = $request->refcode;
    // $order->executive = $referrer->id;
    // $order->manager = $referrer->up_line;
    // $order->group = $referrer->group;
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->string('chart');
            $table->string('status');
            $table->string('payment_status');
            $table->string('name');
            $table->string('address');
            $table->string('state');
            $table->integer('balance');
            $table->string('refcode');
            $table->string('executive');
            $table->string('manager');
            $table->string('group');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
