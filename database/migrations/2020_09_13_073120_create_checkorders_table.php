<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCheckordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checkorders', function (Blueprint $table) {
            $table->id();
            $table->string('chart');
            $table->string('status');
            $table->string('payment_status');
            $table->string('name');
            $table->string('address');
            $table->string('state');
            $table->integer('amount');
            $table->string('refcode');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checkorders');
    }
}
